import {MigrationInterface, QueryRunner} from "typeorm";

export class createUsers1607612703046 implements MigrationInterface {

  public async up(queryRunner: QueryRunner): Promise<void> {
    const users = [];
    for(let i = 0;i < 100;i++) {
      users.push({
        name: `name-${i}`,
        surname: `surname-${i}`,
        address: `address-${i}`
      });
    }

    await queryRunner
      .manager
      .createQueryBuilder()
      .insert()
      .into('users')
      .values(users)
      .execute();
  }

  public async down(queryRunner: QueryRunner): Promise<void> {
    await queryRunner.dropSchema('users');
  }
}
