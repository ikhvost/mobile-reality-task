import {Body, Controller, Get, Param, Post, Put} from '@nestjs/common';
import {UserDto} from './dto/user.dto';
import {UserIdDto} from './dto/user-id.dto';
import {PaginationDto} from './dto/pagination.dto';
import {UserEntity} from './user.entity';
import {UserService} from './user.service';

@Controller('users')
export class UserController {
  constructor(private readonly userService: UserService) {
  }

  @Get()
  public getAll(@Body() dto: PaginationDto): Promise<UserEntity[]> {
    return this.userService.getAll(dto.count, dto.page);
  }

  @Post()
  public create(@Body() dto: UserDto): Promise<UserEntity> {
    return this.userService.create(dto);
  }

  @Put(':id')
  public update(@Param() param: UserIdDto,
                @Body() dto: UserDto): Promise<UserEntity> {
    return this.userService.update(param.id, dto);
  }
}
