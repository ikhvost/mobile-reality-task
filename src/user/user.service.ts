import {Injectable, NotFoundException} from '@nestjs/common';
import {UserDto} from './dto/user.dto';
import {UserEntity} from './user.entity';
import {UserRepository} from './user.repository';

@Injectable()
export class UserService {
  constructor(private userRepository: UserRepository) {
  }

  public getAll(count = 10, page = 0): Promise<UserEntity[]> {
    return this.userRepository.findAndCount({take: count, skip: count * page})
      .then(([users, total]) => users);
  }

  public create(dto: UserDto): Promise<UserEntity> {
    return this.userRepository.save(dto);
  }

  public update(id: number, dto: UserDto): Promise<UserEntity> {
    const user = this.userRepository.findOne({id});
    if(!user) throw new NotFoundException('User not found');
    return this.userRepository.save(dto);
  }
}
