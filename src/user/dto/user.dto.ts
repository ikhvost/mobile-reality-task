import {IsNotEmptyString} from '../../common/is-not-empty-string.validator';

export class UserDto {
  @IsNotEmptyString()
  name: string;

  @IsNotEmptyString()
  surname: string;

  @IsNotEmptyString()
  address: string;
}