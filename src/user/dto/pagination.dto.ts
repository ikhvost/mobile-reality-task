import {Transform} from 'class-transformer';

export class PaginationDto {
  @Transform((value: string, object: Partial<PaginationDto>) => object.count = +value)
  count: number;

  @Transform((value: string, object: Partial<PaginationDto>) => object.page = +value)
  page: number;
}
