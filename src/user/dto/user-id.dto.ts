import {Transform} from 'class-transformer';

export class UserIdDto {
  @Transform((value: string, object: Partial<UserIdDto>) => object.id = +value)
  id: number;
}
