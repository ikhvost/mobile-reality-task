import {Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

@Entity('users')
export class UserEntity {
  @PrimaryGeneratedColumn({type: 'int'} )
  id: number;

  @Column()
  name: string;

  @Column()
  surname: string;

  @Column()
  address: string;
}
