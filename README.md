# Mobile Reality Recruitment Task

### Run with docker
1. Run `docker-compose up --build` (will be used `.env` config).
2. API will be available on `http://APP_HOST:APP_PORT` (by default `http://localhost:3000`).